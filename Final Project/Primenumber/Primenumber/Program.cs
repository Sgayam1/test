﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Primenumber
{
    class Program
    {
        static void Main(string[] args)
        {

            int i;
            int num1;
            Console.WriteLine("Enter Value:");
            num1 = Convert.ToInt32(Console.ReadLine());

            for (i = 1; i <= num1; i++)
            {
                int counter = 0;
                for (int j = 2; j <= i / 2; j++)
                {
                    if (i % j == 0)
                    {
                        counter++;
                        break;
                    }
                }

                if (counter == 0)
                {
                
                    Console.Write("{0} ", i);
                }
            }
            Console.ReadLine();

        }

    }
}
